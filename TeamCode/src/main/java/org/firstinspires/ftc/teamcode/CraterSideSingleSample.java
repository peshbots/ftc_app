package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name="CraterSideSingleSample", group="Linear OpMode")
public class CraterSideSingleSample extends ParameterizedAutoOp {

    @Override
    public void runOpMode() {
        this.isCraterSide = true;
        this.doubleSample = false;
        super.runOpMode();
    }
}
