package org.firstinspires.ftc.teamcode;

import com.gitlab.peshbots.DcMotorPartAuto;
import com.gitlab.peshbots.DoubleCRServo;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

@Autonomous(name = "Depot Side", group = "Autonomous")
public class DepotSideAuto extends LinearOpMode {

    private ElapsedTime runTime = new ElapsedTime();
    private DcMotor lf, rf, lb, rb;
    private DcMotorPartAuto lift, latch, slide, intake;
    private DoubleCRServo flip;


    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private static final String VUFORIA_KEY = "ATpx/+X/////AAABmfb8JPlVBE36u1aOuMQwZNonagKlWLZlN8El54CvVoSwP8/jRizuHUxhU3ZqEEVXaImHF3XrM9cVIAEEj240Wr1FpNwfVcyBrYnDk5O8f5qPSVBLuURXxOvdAB8lP/yC3qPVFN5LKM4XkFBF1JPUHYpOR/O7Vb9JCcubzP8Xi4+qV5jdT9cb5qaUQRzDnnCjkRlZRc10ZpIVIeyerC5W0S+rSAAo9d1StcRyyAaebnpYRCqhZ5C+PCwGARuTSwCRdz424bfPK8G5XZ82Mtklufuf7w7uDDxBg1yoxYhtJNa8TD5bEK7VTPsNYQ0bUGZoD90Ga6HhGwyv/ofKH/3afMJouu0FnzAt1QDhGR4nPU1H";
    protected VuforiaLocalizer vuforia;
    protected TFObjectDetector tfod;

    private BNO055IMU imu;
    protected CameraName cameraName;

    @Override
    public void runOpMode(){

        //HARDWARE MAP/ENCODER RESET
        lf = hardwareMap.get(DcMotor.class, "lf");
        rf = hardwareMap.get(DcMotor.class, "rf");
        lb = hardwareMap.get(DcMotor.class, "lb");
        rb = hardwareMap.get(DcMotor.class, "rb");
        lift = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "lift"));
        slide = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "slide"));
        flip = new DoubleCRServo(hardwareMap.get(CRServo.class, "flipl"), hardwareMap.get(CRServo.class, "flipr"));
        latch = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "latcher"), true);
        intake = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "intake"));

        ResetEncoders();

        lf.setDirection(DcMotor.Direction.REVERSE);
        lb.setDirection(DcMotor.Direction.REVERSE);
        rf.setDirection(DcMotor.Direction.FORWARD);
        rb.setDirection(DcMotor.Direction.FORWARD);
        latch.setDirection(DcMotor.Direction.REVERSE);
        lift.setDirection(DcMotor.Direction.REVERSE);
        slide.setDirection(DcMotor.Direction.FORWARD);
        intake.setDirection(DcMotor.Direction.FORWARD);
        flip.setDirection(CRServo.Direction.FORWARD, CRServo.Direction.REVERSE);

        //

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.loggingEnabled      = false;

        imu = hardwareMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);

        cameraName = hardwareMap.get(CameraName.class, "Webcam");

        double baseAngle = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;


        telemetry.addLine("Ready to latch");
        telemetry.update();



        initVuforia();
        initTfod();


        // Wait for the game to start (driver presses PLAY)
        tfod.activate();
        int goldMinPos = getGoldMineralPosAndWaitForStart();
        if (! opModeIsActive()) return;
        tfod.deactivate();


        telemetry.addData("heading", imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle);
        telemetry.update();


        //UNLATCH
        while (latch.getCurrentPosition() < 27000){

            latch.setPower(1);
        }
        latch.setPower(0);

        //GET OUT OF LATCH
        EncoderDrive(.3, 2,2, 2);
        StrafeEncoderDrive(.5,-8,-8,2);
        EncoderDrive(.3, -2,-2, 2);

        //FACE STRAIGHT AND DEPOSIT MARKER WITH ARM
        AbsoluteTurn(.5, baseAngle + 72);

        while (slide.getCurrentPosition() < 4400){

            slide.setPower(1);
        }
        slide.setPower(0);


        flip.setPower(1);
        WaitFor(1.5);
        flip.setPower(0);


        intake.setPower(-1);
        WaitFor(1);
        intake.setPower(0);


        flip.setPower(-1);
        WaitFor(2);
        flip.setPower(0);


        while (slide.getCurrentPosition() > 100){

            slide.setPower(-1);
        }
        slide.setPower(0);

        //SAMPLING
        telemetry.addData("pos", goldMinPos);
        telemetry.update();

        baseAngle = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;
        switch (goldMinPos) {
            case(0):

                TimeTurn(-.3, .4);
                TimeDrive(.5, .75);
                TimeDrive(-.5, .75);
                TimeTurn(.3,.4);
                break;
            case(1):
                TimeDrive(.5, .5);
                TimeDrive(-.5, .5);
                break;
            case(2):
                AbsoluteTurn(.3, baseAngle -35);
                TimeDrive(.5, .75);
                TimeDrive(-.5, .75);

                break;

        }

        WaitFor(.5);

        //FACE WALL AND DRIVE
        AbsoluteTurn(.3, baseAngle -70);

        WaitFor(.5);

        EncoderDrive(.7,17,17,3);

        //TURN TO CRATER AND EXTEND ARM
        AbsoluteTurn(.3, baseAngle-120);

        while (slide.getCurrentPosition() < 4400){

            slide.setPower(1);
        }
        slide.setPower(0);
    }


    private void ResetEncoders(){

        lf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        latch.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


        lf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        slide.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        latch.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }


    protected void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
//        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        parameters.cameraName = cameraName;

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }

    /**
     * Initialize the Tensor Flow Object Detection engine.
     */
    protected void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }

    protected int getGoldMineralPosAndWaitForStart() {
        int goldMineralPos = 2;
        while (/*opModeIsActive() && */! isStarted() && ! isStopRequested()) {
            if (tfod != null) {
                // getUpdatedRecognitions() will return null if no new information is available since
                // the last time that call was made.
                List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    telemetry.addData("# Object Detected", updatedRecognitions.size());
                    int goldMineralCount = 0;
                    for (Recognition recognition : updatedRecognitions) {
                        /* note: the following conditions mean:
                            recognition.getWidth() < recognition.getImageWidth() / 3
                                avoids a very wide false positive that can be caused by the background
                            recognition.getBottom() > recognition.getImageHeight() * 2 / 3
                                ignores any minerals in the crater
                            recognition.getWidth() < 1.5 * recognition.getHeight()
                                avoids a rectangular false positive generated by the red x
                        */
                        if (recognition.getWidth() < recognition.getImageWidth() / 3 &&
                                recognition.getBottom() > recognition.getImageHeight() * 2 / 3 &&
                                recognition.getWidth() < 1.5 * recognition.getHeight()) {
                            if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                goldMineralCount++;
                                if (recognition.getLeft() < recognition.getImageWidth() / 3) {
                                    goldMineralPos = 0;
                                }
                                else if (recognition.getLeft() < recognition.getImageWidth() / 3 * 2) {
                                    goldMineralPos = 1;
                                }
                                else {
                                    goldMineralPos = 2;
                                }
                            }
                        }
                    }
                    if (goldMineralCount <= 1) {
                        if (goldMineralPos == 0) {
                            telemetry.addData("Gold Mineral Position", "Left");
                        } else if (goldMineralPos == 1) {
                            telemetry.addData("Gold Mineral Position", "Center");
                        } else if (goldMineralPos == 2) {
                            telemetry.addData("Gold Mineral Position", "Right");
                        }
                    } else {
                        goldMineralPos = 2;
                    }
                    telemetry.update();
                }
            }
        }
        return goldMineralPos;
    }


    //TIME METHODS-------------------------------------------------------------------------------
    void WaitAbsolute(double seconds) {

        while (opModeIsActive() && runTime.seconds() <= seconds) {
            if(!opModeIsActive()){
                StopDriveMotors();
                break;
            }
            telemetry.addData("Time Remaining ", Math.ceil(seconds - runTime.seconds()));
            telemetry.update();
            telemetry.addData("Current Time ", runTime.seconds());
            telemetry.update();
            idle();
        }
        if(!opModeIsActive())
            stop();
    }

    double getNewTime(double addedSeconds) {
        return runTime.seconds() + addedSeconds;
    }

    /**
     * wait for a given time in seconds
     * @param seconds
     */
    void WaitFor(double seconds) {
        WaitAbsolute(getNewTime(seconds));
    }


    //MY NAV METHODS

    void TimeDrive(double speed, double time){
        lf.setPower(speed);
        lb.setPower(speed);
        rf.setPower(speed);
        rb.setPower(speed);

        WaitFor(time);

        StopDriveMotors();

    }
    void TimeTurn(double speed, double time){

        lf.setPower(speed);
        lb.setPower(speed);
        rf.setPower(-speed);
        rb.setPower(-speed);

        WaitFor(time);

        StopDriveMotors();

    }
    void AbsoluteTurn(double speed, double targetAngle){

        double currentAngle = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;


        if (currentAngle < targetAngle){

            while (opModeIsActive() && imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle < targetAngle) {

                lf.setPower(-speed);
                rf.setPower(speed);
                lb.setPower(-speed);
                rb.setPower(speed);
            }


        }else if (currentAngle > targetAngle){

            while (opModeIsActive() && imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle > targetAngle) {

                lf.setPower(speed);
                rf.setPower(-speed);
                lb.setPower(speed);
                rb.setPower(-speed);
            }
        }

        StopDriveMotors();

    }

    //encoder constants
    static final double     COUNTS_PER_MOTOR_REV    = 2240 ;    // change for mecanum
    static final double     DRIVE_GEAR_REDUCTION    = 1.0 ;     // This is < 1.0 if geared UP
    static final double     WHEEL_DIAMETER_INCHES   = 4 ;     // For figuring circumference
    static final double     COUNTS_PER_INCH         = (COUNTS_PER_MOTOR_REV * DRIVE_GEAR_REDUCTION) /
            (WHEEL_DIAMETER_INCHES * 3.1415);

    void EncoderDrive(double speed, double leftInches, double rightInches, double timeout) {
        ElapsedTime runtime = new ElapsedTime();

        int newLeftTarget;
        int newRightTarget;

        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newLeftTarget = lf.getCurrentPosition() + (int) ((leftInches * COUNTS_PER_INCH));
            newRightTarget = rf.getCurrentPosition() + (int) ((rightInches * COUNTS_PER_INCH));
            lf.setTargetPosition(newLeftTarget);
            rf.setTargetPosition(newLeftTarget);
            lb.setTargetPosition(newRightTarget);
            rb.setTargetPosition(newRightTarget);

            // Turn On RUN_TO_POSITION
            lf.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rf.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            lb.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rb.setMode(DcMotor.RunMode.RUN_TO_POSITION);


            // reset the timeout time and start motion.
            runtime.reset();

            lf.setPower(Math.abs(speed));
            rf.setPower(Math.abs(speed));
            lb.setPower(Math.abs(speed));
            rb.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeout) &&
                    (lf.isBusy() && rf.isBusy())) {

                // Display it for the driver.
                telemetry.addData("Path1", "Running to %7d :%7d", newLeftTarget, newRightTarget);
                telemetry.addData("Path2", "Running at %7d :%7d",
                        lf.getCurrentPosition(),
                        rf.getCurrentPosition());
                telemetry.update();
            }

            // Stop all motion;
            StopDriveMotors();

            // Turn off RUN_TO_POSITION
            lf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            rf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            lb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            rb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


            lf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            lb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        }
    }


    void StrafeEncoderDrive(double speed, double leftInches, double rightInches, double timeout) {
        ElapsedTime runtime = new ElapsedTime();

        int newLeftTarget;
        int newRightTarget;

        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newLeftTarget = lf.getCurrentPosition() + (int) ((leftInches * COUNTS_PER_INCH));
            newRightTarget = rb.getCurrentPosition() + (int) ((rightInches * COUNTS_PER_INCH));
            lf.setTargetPosition(newLeftTarget);
            rf.setTargetPosition(-newLeftTarget);
            lb.setTargetPosition(-newRightTarget);
            rb.setTargetPosition(newRightTarget);

            // Turn On RUN_TO_POSITION
            lf.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rf.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            lb.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rb.setMode(DcMotor.RunMode.RUN_TO_POSITION);


            // reset the timeout time and start motion.
            runtime.reset();

            lf.setPower(Math.abs(speed));
            rf.setPower(Math.abs(speed));
            lb.setPower(Math.abs(speed));
            rb.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeout) &&
                    (lf.isBusy() && rf.isBusy())) {

                // Display it for the driver.
                telemetry.addData("Path1", "Running to %7d :%7d", newLeftTarget, newRightTarget);
                telemetry.addData("Path2", "Running at %7d :%7d",
                        lf.getCurrentPosition(),
                        rf.getCurrentPosition());
                telemetry.update();
            }

            // Stop all motion;
            StopDriveMotors();

            // Turn off RUN_TO_POSITION
            lf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            rf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            lb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            rb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


            lf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            lb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        }
    }

    private void StopDriveMotors(){

        lf.setPower(0);
        rf.setPower(0);
        lb.setPower(0);
        rb.setPower(0);
    }

}
