/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.gitlab.peshbots.DcMotorPartAuto;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

/**
 * This file contains an emagnitude *aMath.mple of an iterative (Non-Linear) "OpMode".
 * An OpMode is a 'program' that runs in either the autonomous or the teleop period of an FTC match.
 * The names of OpModes appear on the menu of the FTC Driver Station.
 * When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and emagnitude *eMath.cuted.
 *
 * This particular OpMode just emagnitude *eMath.cutes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all iterative OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@Disabled // not implemented in time for competition :(
@TeleOp(name="RealTeleOp", group="Iterative Opmode")

public class RealTeleOp extends OpMode
{
    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftFrontDrive = null;
    private DcMotor rightFrontDrive = null;
    private DcMotor leftBackDrive = null;
    private DcMotor rightBackDrive = null;
    private DcMotorPartAuto lifterMotor = null;
    private DcMotorPartAuto sliderMotor = null;
    private DcMotorPartAuto clawFlipperMotor = null;
    private DcMotorPartAuto scooperMotor = null;
    private Servo leftFlipper = null;
    private Servo rightFlipper = null;
    private NormalizedColorSensor colorSensor = null;
    private DistanceSensor distanceSensor = null;
    private boolean wasXReleased = true;
    private boolean wasStickReleased = true;
    private boolean flipStatus = false;
    private boolean preciseMode = false;


    private void resetMotors() {
        leftFrontDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftBackDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightFrontDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBackDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftFrontDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftBackDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightFrontDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightBackDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftFrontDrive.setDirection(DcMotor.Direction.FORWARD);
        leftBackDrive.setDirection(DcMotor.Direction.FORWARD);
        rightFrontDrive.setDirection(DcMotor.Direction.REVERSE);
        rightBackDrive.setDirection(DcMotor.Direction.REVERSE);

        lifterMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lifterMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lifterMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lifterMotor.setDirection(DcMotor.Direction.REVERSE);

        sliderMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        sliderMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        sliderMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        sliderMotor.setDirection(DcMotor.Direction.FORWARD);

        clawFlipperMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        clawFlipperMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        clawFlipperMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        clawFlipperMotor.setDirection(DcMotor.Direction.FORWARD);

        scooperMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        scooperMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        scooperMotor.setDirection(DcMotor.Direction.FORWARD);
    }

    /*
     * Code to run ONCE when the driver hits INIT
     */
    @Override
    public void init() {
        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFrontDrive = hardwareMap.get(DcMotor.class, "leftfdrive");
        rightFrontDrive = hardwareMap.get(DcMotor.class, "rightfdrive");
        leftBackDrive = hardwareMap.get(DcMotor.class, "leftbdrive");
        rightBackDrive = hardwareMap.get(DcMotor.class, "rightbdrive");
        lifterMotor = hardwareMap.get(DcMotorPartAuto.class, "lifter"); // WARNING: THIS NEEDS TO BE TESTED!
        sliderMotor = hardwareMap.get(DcMotorPartAuto.class, "slider");
        clawFlipperMotor = hardwareMap.get(DcMotorPartAuto.class, "clawFlipper");
        scooperMotor = hardwareMap.get(DcMotorPartAuto.class, "scooperMotor");
        leftFlipper = hardwareMap.get(Servo.class, "lflipper");
        rightFlipper = hardwareMap.get(Servo.class, "rflipper");
        colorSensor = hardwareMap.get(NormalizedColorSensor.class, "color");
        distanceSensor = hardwareMap.get(DistanceSensor.class, "color");

        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery

        resetMotors();

        // Tell the driver that initialization is complete.
        telemetry.addData("Status", "Initialized");
        telemetry.update();
    }

    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    @Override
    public void init_loop() {
    }

    /*
     * Code to run ONCE when the driver hits PLAY
     */
    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
     */
    @Override
    public void loop() {
        telemetry.addData("Status", "running");
        // Setup a variable for each drive wheel to save power level for telemetry

        // Choose to drive using either Tank Mode, or POV Mode
        // Comment out the method that's not used.  The default below is POV.

        // POV Mode uses left stick to go forward, and right stick to turn.
        // - This uses basic math to combine motions and is easier to drive straight.
        double magnitude = Math.sqrt(Math.pow(gamepad1.left_stick_x, 2) + Math.pow(gamepad1.left_stick_y, 2));
        double direction = Math.atan2(-gamepad1.left_stick_x, gamepad1.left_stick_y);
        double rotation = -gamepad1.right_stick_x;
        if (gamepad1.left_stick_button || gamepad1.right_stick_button) { // precise mode
            if (wasStickReleased) {
                wasStickReleased = false;
                preciseMode = ! preciseMode;
            }
        }
        else wasStickReleased = true; // this is necessary because otherwise it will perform the same toggle action each time the loop iterates WHILE the button is pressed
				// this debounces it so that it doesn't register a press until it is released
        if (preciseMode) magnitude *= 0.5;
        telemetry.addData("precise", preciseMode);

        // Tank Mode uses one stick to control each wheel.
        // - This requires no math, but it is hard to drive forward slowly and keep straight.
        // leftPower  = -gamepad1.left_stick_y ;
        // rightPower = -gamepad1.right_stick_y ;

        // Send calculated power to wheels
        leftFrontDrive.setPower(magnitude * Math.sin(direction + Math.PI/4) + rotation);
        leftBackDrive.setPower(magnitude * Math.cos(direction + Math.PI/4) + rotation);
        rightFrontDrive.setPower(magnitude * Math.cos(direction + Math.PI/4) - rotation);
        rightBackDrive.setPower(magnitude * Math.sin(direction + Math.PI/4) - rotation);
        telemetry.addData("lfd pos", leftFrontDrive.getCurrentPosition());
        telemetry.addData("lbd pos", leftBackDrive.getCurrentPosition());
        telemetry.addData("rfd pos", rightFrontDrive.getCurrentPosition());
        telemetry.addData("rbd pos", rightBackDrive.getCurrentPosition());

        scooperMotor.setPower((gamepad2.right_trigger - gamepad2.left_trigger) * 0.75);
        telemetry.addData("scoop pow", scooperMotor.getPowerFloat());

        if (Math.abs(gamepad2.right_stick_y) > 0.25) clawFlipperMotor.setPower(gamepad2.right_stick_y);
        else clawFlipperMotor.setPower(0);
        telemetry.addData("clawfm pos", clawFlipperMotor.getCurrentPosition());

        if (Math.abs(gamepad2.right_stick_x) > 0.25) sliderMotor.setPower(gamepad2.right_stick_x);
        else sliderMotor.setPower(0);
        telemetry.addData("slidem pos", sliderMotor.getCurrentPosition());

        if (Math.abs(gamepad2.left_stick_y) > 0.25) lifterMotor.setPower(gamepad2.left_stick_y);
        else lifterMotor.setPower(0);
        telemetry.addData("liftm pos", lifterMotor.getCurrentPosition());

        if (gamepad2.x) {
            if (wasXReleased) {
                wasXReleased = false;
                flipStatus = ! flipStatus;
                leftFlipper.setPosition(flipStatus ? 0.1 : 1.0); // the left servo is mounted the opposite way so its values are reversed
                rightFlipper.setPosition(flipStatus ? 0.9 : 0.0);
            }
        }
        else wasXReleased = true; // more debouncing
        telemetry.addData("leftflip", leftFlipper.getPosition());
        telemetry.addData("rightflip", rightFlipper.getPosition());

        NormalizedRGBA colors = colorSensor.getNormalizedColors();
        telemetry.addData("r g b a", colors.red + " " + colors.green + " " + colors.blue + " " + colors.alpha);
        double dist = distanceSensor.getDistance(DistanceUnit.INCH);
        telemetry.addData("dist", dist);

        if (gamepad1.right_bumper) {
            resetMotors();
        }

        telemetry.update();
    }

    /*
     * Code to run ONCE after the driver hits STOP
     */
    @Override
    public void stop() { // ensure all motors stop at the end
			leftFrontDrive.setPower(0);
			leftBackDrive.setPower(0);
			rightFrontDrive.setPower(0);
			rightBackDrive.setPower(0);
			lifterMotor.setPower(0);
			sliderMotor.setPower(0);
			clawFlipperMotor.setPower(0);
			scooperMotor.setPower(0);
    }

}
