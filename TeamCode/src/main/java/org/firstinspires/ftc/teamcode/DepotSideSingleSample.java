package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name="DepotSideSingleSample", group="Linear OpMode")
public class DepotSideSingleSample extends ParameterizedAutoOp {

    @Override
    public void runOpMode() {
        this.isCraterSide = false;
        this.doubleSample = false;
        super.runOpMode();
    }
}
