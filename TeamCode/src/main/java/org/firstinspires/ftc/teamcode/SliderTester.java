package org.firstinspires.ftc.teamcode;

import com.gitlab.peshbots.DcMotorPartAuto;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

//import ParameterizedAutoOp;

@Autonomous(name="Slider tester", group="Sensor")
public class SliderTester extends ParameterizedAutoOp {
    private DcMotorPartAuto sliderMotor = null;

    @Override
    public void runOpMode() {
        sliderMotor = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "slider"));
        sliderMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        sliderMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        sliderMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        sliderMotor.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        while (opModeIsActive()) {
            realEncoderRotateMotorToPosition(1.0, sliderMotor, 89, 3); // extend and retract the slider to eat the mineral
            realEncoderRotateMotorToPosition(1.0, sliderMotor, 0, 3);
        }
    }
}
