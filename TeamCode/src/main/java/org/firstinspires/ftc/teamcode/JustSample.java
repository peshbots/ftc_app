package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name="JustSample", group="Linear OpMode")
public class JustSample extends ParameterizedAutoOp {

    @Override
    public void runOpMode() {
        this.isCraterSide = true;
        this.doubleSample = false;
        this.sampleOnly = true;
        super.runOpMode();
    }
}
