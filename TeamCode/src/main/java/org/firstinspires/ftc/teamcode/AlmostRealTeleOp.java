/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.gitlab.peshbots.DoubleCRServo;
import com.gitlab.peshbots.MotorPath;
import com.qualcomm.ftccommon.SoundPlayer;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.gitlab.peshbots.DcMotorPartAuto;

/**
 * This file contains an emagnitude *aMath.mple of an iterative (Non-Linear) "OpMode".
 * An OpMode is a 'program' that runs in either the autonomous or the teleop period of an FTC match.
 * The names of OpModes appear on the menu of the FTC Driver Station.
 * When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and emagnitude *eMath.cuted.
 *
 * This particular OpMode just emagnitude *eMath.cutes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all iterative OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="AlmostRealTeleOp", group="Iterative Opmode")

public class AlmostRealTeleOp extends OpMode
{
    private static int COUNTS_PER_INTAKE_REV = 420;

    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor lf = null;
    private DcMotor rf = null;
    private DcMotor lb = null;
    private DcMotor rb = null;
    private DcMotorPartAuto lift = null;
    private DcMotorPartAuto latch = null;
    private DcMotorPartAuto slide = null;
    DoubleCRServo flip;
    private DcMotorPartAuto intake = null;
    private Servo dump = null;
    private Servo block = null;
//    private NormalizedColorSensor colorSensor = null;
//    private DistanceSensor distanceSensor = null;
    private boolean wasX2Released = true;
    private boolean wasY2Released = true;
    private boolean wasB2Released = true;
    private boolean wasA2Released = true;
    private boolean wasStick1Released = true;
    private double prevIntakePower = 0;
    private boolean flipStatus = false;
    private boolean blockStatus = true;
    private boolean preciseMode = false;
    private DcMotor[] motors;
    private DcMotorPartAuto[] limitedMotors;

    private int pbResID;

    private MotorPath extend_unextend_claw;

    /** Resets all of the motors to their default state
     *  @param encoders whether or not to reset the encoders to zero also
     */
    private void resetMotors(boolean encoders) {
        for (DcMotor m: motors) {
            if (encoders) {
                m.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                m.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
            m.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        }
        lf.setDirection(DcMotor.Direction.FORWARD);
        lb.setDirection(DcMotor.Direction.FORWARD);
        rf.setDirection(DcMotor.Direction.REVERSE);
        rb.setDirection(DcMotor.Direction.REVERSE);
        latch.setDirection(DcMotor.Direction.REVERSE);
        lift.setDirection(DcMotor.Direction.REVERSE);
        slide.setDirection(DcMotor.Direction.FORWARD);
        intake.setDirection(DcMotor.Direction.FORWARD);
        flip.setDirection(CRServo.Direction.FORWARD, CRServo.Direction.REVERSE);
    }

    /*
     * Code to run ONCE when the driver hits INIT
     */
    @Override
    public void init() {
        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        lf = hardwareMap.get(DcMotor.class, "lf");
        rf = hardwareMap.get(DcMotor.class, "rf");
        lb = hardwareMap.get(DcMotor.class, "lb");
        rb = hardwareMap.get(DcMotor.class, "rb");
        lift = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "lift"));
        slide = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "slide"));
        flip = new DoubleCRServo(hardwareMap.get(CRServo.class, "flipl"), hardwareMap.get(CRServo.class, "flipr"));
        latch = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "latcher"), true);
        dump = hardwareMap.get(Servo.class, "flipperservo");
        intake = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "intake"));
        block = hardwareMap.get(Servo.class, "block");
        block.setPosition(1);
        pbResID = hardwareMap.appContext.getResources().getIdentifier("peshbots", "raw", hardwareMap.appContext.getPackageName());
        motors = new DcMotor[]{lf, rf, lb, rb, lift, latch, slide, intake};
        limitedMotors = new DcMotorPartAuto[]{lift, latch, slide};
        SoundPlayer.getInstance().preload(hardwareMap.appContext, pbResID);
//        colorSensor = hardwareMap.get(NormalizedColorSensor.class, "color");
//        distanceSensor = hardwareMap.get(DistanceSensor.class, "color");

        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery

        resetMotors(false);

        // set motor limits
        latch.setLimits(0, 29000);
        lift.setLimits(-6100, 0);
        slide.setLimits(0, 4800);


        // TODO: test motor path

        // Tell the driver that initialization is complete.
        telemetry.addData("Status", "Initialized");
        telemetry.update();
    }

    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    @Override
    public void init_loop() {
    }

    /*
     * Code to run ONCE when the driver hits PLAY
     */
    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
     */
    @Override
    public void loop() {
        telemetry.addData("Status", "running");
        // Setup a variable for each drive wheel to save power level for telemetry

        // Choose to drive using either Tank Mode, or POV Mode
        // Comment out the method that's not used.  The default below is POV.

        // POV Mode uses left stick to go forward, and right stick to turn.
        // It's kinda complicated with mecanum wheels
        double magnitude = Math.sqrt(Math.pow(gamepad1.left_stick_x, 2) + Math.pow(gamepad1.left_stick_y, 2));
        double direction = Math.atan2(-gamepad1.left_stick_x, gamepad1.left_stick_y);
        double rotation = -gamepad1.right_stick_x;
        if (gamepad1.left_stick_button || gamepad1.right_stick_button) { // precise mode
            if (wasStick1Released) {
                wasStick1Released = false;
                preciseMode = ! preciseMode;
            }
        }
        else wasStick1Released = true; // this is necessary because otherwise it will perform the same toggle action each time the loop iterates WHILE the button is pressed
        // this debounces it so that it doesn't register a press until it is released
        if (preciseMode) magnitude *= 0.5;
        telemetry.addData("precise", preciseMode);

        // Send calculated power to wheels
        lf.setPower(magnitude * Math.sin(direction + Math.PI/4) + rotation);
        lb.setPower(magnitude * Math.cos(direction + Math.PI/4) + rotation);
        rf.setPower(magnitude * Math.cos(direction + Math.PI/4) - rotation);
        rb.setPower(magnitude * Math.sin(direction + Math.PI/4) - rotation);
        telemetry.addData("lfd pos", lf.getCurrentPosition());
        telemetry.addData("lbd pos", lb.getCurrentPosition());
        telemetry.addData("rfd pos", rf.getCurrentPosition());
        telemetry.addData("rbd pos", rb.getCurrentPosition());

        // spin the thing that gathers minerals
//        if (gamepad2.right_trigger > 0.1 || gamepad2.left_trigger > 0.1) {
        double intakePower = (gamepad2.right_trigger - gamepad2.left_trigger);
//        if (intakePower == 0.0 && prevIntakePower != 0 && intake.getCurrentPosition() % COUNTS_PER_INTAKE_REV != 0) {
//            int continueTo;
//            if (prevIntakePower < 0) {
//                continueTo = intake.getCurrentPosition() / COUNTS_PER_INTAKE_REV * COUNTS_PER_INTAKE_REV - COUNTS_PER_INTAKE_REV;
//            }
//            else /*if (prevIntakePower > 0) */{
//                continueTo = intake.getCurrentPosition() / COUNTS_PER_INTAKE_REV * COUNTS_PER_INTAKE_REV + COUNTS_PER_INTAKE_REV;
//            }
//            intake.autoRunToPosition(1.0, continueTo, 2000);
//        }
//        prevIntakePower = intakePower;
        intake.setPowerOrAutoRun(intakePower);
        telemetry.addData("scoop pos", intake.getCurrentPosition());

        if (Math.abs(gamepad2.left_stick_y) > 0.25) flip.setPower(gamepad2.left_stick_y);
        // there's a 25% deadzone on the x and y axes separately on these sticks
        // that way if you mean to move it only vertically, that's how the robot will perceive it too
        else flip.setPower(0);
//        telemetry.addData("clawfm pos", flip.getCurrentPosition());

        if (Math.abs(gamepad2.right_stick_x) > 0.25) slide.setPowerOrAutoRun(gamepad2.right_stick_x);
        else slide.setPowerOrAutoRun(0);
        telemetry.addData("slidem pos", slide.getCurrentPosition());

        lift.setPowerOrAutoRun((gamepad1.left_trigger - gamepad1.right_trigger));
        telemetry.addData("liftm pos", lift.getCurrentPosition());

        if (gamepad2.x /*|| gamepad1.x*/) { // dump the dumper thing
            if (wasX2Released) {
                wasX2Released = false;
                flipStatus = ! flipStatus;
                dump.setPosition(flipStatus ? 1.0 : 0.0); // the left servo is mounted the opposite way so its values are reversed
            }
        }
        else wasX2Released = true; // more debouncing
        telemetry.addData("flipserv", dump.getPosition());

//        if (gamepad2.y) { // move the lifter to latching height
//            if (wasY2Released) {
//                wasY2Released = false;
//                latch.autoRunToPosition(0.75, 26500, 10000);
//            }
//        }
//        else wasY2Released = true; // more debouncing

        if (gamepad2.b) { // block the claw
            if (wasB2Released) {
                wasB2Released = false;
                blockStatus = ! blockStatus;
                block.setPosition(blockStatus ? 1.0 : 0.0);
            }
        }
        else wasB2Released = true; // more debouncing

        if (gamepad2.a || gamepad1.a) {
            if (wasA2Released) {
                wasA2Released = false;
                SoundPlayer.getInstance().startPlaying(hardwareMap.appContext, pbResID);
            }
        }
        else wasA2Released = true;

        double latcherPower = (gamepad1.dpad_up ? 1.0 : 0.0) - (gamepad1.dpad_down ? 1.0 : 0.0);
        telemetry.addData("latcher pow", latcherPower);
        boolean latchDidRun = latch.setPowerOrAutoRun(latcherPower);
        telemetry.addData("latcher ran", latchDidRun);
        telemetry.addData("latcher pos", latch.getCurrentPosition());

//        NormalizedRGBA colors = colorSensor.getNormalizedColors(); // get the colors (for debugging)
//        telemetry.addData("r g b a", colors.red + " " + colors.green + " " + colors.blue + " " + colors.alpha);
//        double dist = distanceSensor.getDistance(DistanceUnit.INCH); // get the distance (for debugging)
//        telemetry.addData("dist", dist);

        //debug shortcuts:
        if ((gamepad1.left_bumper && gamepad1.right_bumper && gamepad1.dpad_left)/* || (gamepad2.left_bumper && gamepad2.right_bumper && gamepad2.dpad_right)*/) {
            // reset the encoders!
            resetMotors(true);
            init();
        }
        else if ((gamepad1.left_bumper && gamepad1.right_bumper && gamepad1.dpad_right)/* || (gamepad2.left_bumper && gamepad2.right_bumper && gamepad2.dpad_up)*/) {
            // disable the limits on the motors
            for (DcMotorPartAuto m: limitedMotors) {
                m.disableLimits();
            }
        }

        /*if (extend_unextend_claw.isRunning() || gamepad2.a) {
            telemetry.addData("runorstart", extend_unextend_claw.runOrStart());
        }*/

        telemetry.update();
    }

    /*
     * Code to run ONCE after the driver hits STOP
     */
    @Override
    public void stop() { // ensure all motors stop at the end
	    for (DcMotor m: motors) {
	        m.setPower(0);
        }
        flip.setPower(0);
    }

}