/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

//import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.gitlab.peshbots.DcMotorPartAuto;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

//import static java.lang.Double.NaN;
import java.util.List;

import static java.lang.Double.isNaN;


/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

public abstract class ParameterizedAutoOp extends LinearOpMode {

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private static final String VUFORIA_KEY = "ATpx/+X/////AAABmfb8JPlVBE36u1aOuMQwZNonagKlWLZlN8El54CvVoSwP8/jRizuHUxhU3ZqEEVXaImHF3XrM9cVIAEEj240Wr1FpNwfVcyBrYnDk5O8f5qPSVBLuURXxOvdAB8lP/yC3qPVFN5LKM4XkFBF1JPUHYpOR/O7Vb9JCcubzP8Xi4+qV5jdT9cb5qaUQRzDnnCjkRlZRc10ZpIVIeyerC5W0S+rSAAo9d1StcRyyAaebnpYRCqhZ5C+PCwGARuTSwCRdz424bfPK8G5XZ82Mtklufuf7w7uDDxBg1yoxYhtJNa8TD5bEK7VTPsNYQ0bUGZoD90Ga6HhGwyv/ofKH/3afMJouu0FnzAt1QDhGR4nPU1H";
    protected VuforiaLocalizer vuforia;
    protected TFObjectDetector tfod;

    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftFrontDrive = null;
    private DcMotor rightFrontDrive = null;
    private DcMotor leftBackDrive = null;
    private DcMotor rightBackDrive = null;
    private DcMotorPartAuto lifterMotor = null;
    private DcMotorPartAuto latcherMotor = null;
    private DcMotorPartAuto sliderMotor = null;
    private DcMotorPartAuto clawFlipperMotor = null;
    private CRServo intakeServo = null;
    private Servo flipperServo = null;
    private BNO055IMU imu = null;
    protected CameraName cameraName = null;
    private float imuInitialHeading = 0;
    private static double COUNTS_PER_INCH_HD_MECANUM = 1120 / Math.PI / 4 * 15 / 20;
    private static int COUNTS_PER_REV_CORE = 288;
    private static double TURN_DISTANCE_PER_DEGREE = Math.sqrt(1560.49) * Math.PI / 360 / 2;
    private static double SQRT2 = Math.sqrt(2);
    private static double COLOR_THRESHOLD = 1.5;
    private static double DISTANCE_THRESHOLD = 20;
    private static double DRIVE_SPEED = 1.0;
    boolean isCraterSide = true;
    boolean doubleSample = false; // these two variables are overridden by subclasses (that's what makes this AutoOp "parameterized")
    boolean sampleOnly = false;

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFrontDrive = hardwareMap.get(DcMotor.class, "leftfdrive");
        rightFrontDrive = hardwareMap.get(DcMotor.class, "rightfdrive");
        leftBackDrive = hardwareMap.get(DcMotor.class, "leftbdrive");
        rightBackDrive = hardwareMap.get(DcMotor.class, "rightbdrive");
        leftFrontDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightFrontDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftBackDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBackDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        sliderMotor = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "slider"));
        sliderMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        sliderMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        sliderMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        sliderMotor.setDirection(DcMotor.Direction.REVERSE);
        lifterMotor = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "loader"));
        lifterMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lifterMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lifterMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        clawFlipperMotor = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "flipper"));
        clawFlipperMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        clawFlipperMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        clawFlipperMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        latcherMotor = new DcMotorPartAuto(hardwareMap.get(DcMotor.class, "latcher"));
        latcherMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        latcherMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        flipperServo = hardwareMap.get(Servo.class, "flipperservo");
        intakeServo = hardwareMap.get(CRServo.class, "intakeservo");
        intakeServo.setDirection(CRServo.Direction.REVERSE);

        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery
//        leftFrontDrive.setDirection(DcMotor.Direction.FORWARD);
//        rightFrontDrive.setDirection(DcMotor.Direction.REVERSE);
//        leftBackDrive.setDirection(DcMotor.Direction.FORWARD);
//        rightBackDrive.setDirection(DcMotor.Direction.REVERSE);
        leftFrontDrive.setDirection(DcMotor.Direction.REVERSE);
        rightFrontDrive.setDirection(DcMotor.Direction.FORWARD);
        leftBackDrive.setDirection(DcMotor.Direction.REVERSE);
        rightBackDrive.setDirection(DcMotor.Direction.FORWARD);

        leftFrontDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightFrontDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftBackDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightFrontDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        imu = hardwareMap.get(BNO055IMU.class, "imu");
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        // parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        imu.initialize(parameters);

        cameraName = hardwareMap.get(CameraName.class, "Webcam");

        initVuforia();
        initTfod();
        while (!imu.isSystemCalibrated()) idle();
        imuInitialHeading = getHeading();

        intakeServo.setPower(0);

        // Wait for the game to start (driver presses PLAY)
        tfod.activate();
        int goldMinPos = getGoldMineralPosAndWaitForStart();
        if (! opModeIsActive()) return;
        tfod.deactivate();

        runtime.reset();

        // lower
        realEncoderRotateMotorToPosition(1.0, latcherMotor, 1530, 6);


        // unlatch
        // Austin says we should rotate instead of strafing
        encoderTurn(DRIVE_SPEED, 15.0, 2.0);
        encoderDrive(DRIVE_SPEED, 3, 3, 2);
        encoderTurn(DRIVE_SPEED, -15, 2);

        telemetry.addData("gold mineral pos", goldMinPos);
        telemetry.update();

        runtime.reset();
//        while (opModeIsActive() && runtime.milliseconds() < 6000);

        realEncoderRotateMotorToPosition(1.0, clawFlipperMotor, 300, 3); // move claw down
        encoderDrive(DRIVE_SPEED, 5, 5, 3); // move forward


        int mineralTurnAngle = 0;
        if (goldMinPos == 0) mineralTurnAngle = -30;
        else if (goldMinPos == 2) mineralTurnAngle = 30;
        encoderTurn(DRIVE_SPEED, mineralTurnAngle, 2); // turn to gather the mineral
        realEncoderRotateMotorToPosition(1.0, sliderMotor, 89, 3); // extend and retract the slider to eat the mineral

        if (! isCraterSide) {
            // depot side auton
            encoderTurn(DRIVE_SPEED, -mineralTurnAngle, 3);
            encoderDrive(DRIVE_SPEED, 7, 7, 3); // move forward
            intakeServo.setPower(1); // start spitting
            waitMs(1000); // wait for the stuff to leave the claw
            intakeServo.setPower(0); // stop spitting

            realEncoderRotateMotorToPosition(1.0, clawFlipperMotor, 150, 3);
            realEncoderRotateMotorToPosition(1.0, sliderMotor, 0, 3);
            realEncoderRotateMotorToPosition(1.0, clawFlipperMotor, 0, 3);

            imuTurn(DRIVE_SPEED, 90); // turn 90 to left of starting pos
        }
        else {
            // nothing
        }

        //while (runtime.seconds() < 10 && opModeIsActive()) idle(); // wait for 10 s
    }

    private void encoderDriveStrafe(double speed,
                                   double frontInches, double backInches,
                                   double timeoutS) {
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newLeftBackTarget;
        int newRightBackTarget;

        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newLeftFrontTarget = leftFrontDrive.getCurrentPosition() + (int)(frontInches * COUNTS_PER_INCH_HD_MECANUM);
            newRightFrontTarget = rightFrontDrive.getCurrentPosition() + (int)(frontInches * COUNTS_PER_INCH_HD_MECANUM * -1);
            newLeftBackTarget = leftBackDrive.getCurrentPosition() + (int)(backInches * COUNTS_PER_INCH_HD_MECANUM) * -1;
            newRightBackTarget = rightBackDrive.getCurrentPosition() + (int)(backInches * COUNTS_PER_INCH_HD_MECANUM);
            leftFrontDrive.setTargetPosition(newLeftFrontTarget);
            rightFrontDrive.setTargetPosition(newRightFrontTarget);
            leftBackDrive.setTargetPosition(newLeftBackTarget);
            rightBackDrive.setTargetPosition(newRightBackTarget);

            // Turn On RUN_TO_POSITION
            leftFrontDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rightFrontDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            leftBackDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rightBackDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();
            leftFrontDrive.setPower(Math.abs(speed));
            rightFrontDrive.setPower(Math.abs(speed));
            leftBackDrive.setPower(Math.abs(speed));
            rightBackDrive.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (leftFrontDrive.isBusy() && rightFrontDrive.isBusy() && leftBackDrive.isBusy() && rightBackDrive.isBusy())) {

                /*// Display it for the driver.
                telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                telemetry.addData("Path2",  "Running at %7d :%7d",
                        robot.leftDrive.getCurrentPosition(),
                        robot.rightDrive.getCurrentPosition());
                telemetry.update();*/
            }

            // Stop all motion;
            leftFrontDrive.setPower(0);
            rightFrontDrive.setPower(0);
            leftBackDrive.setPower(0);
            rightBackDrive.setPower(0);

            // Turn off RUN_TO_POSITION
            leftFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rightFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            leftBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rightBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }


    /*
     *  Method to perfmorm a relative move, based on encoder counts.
     *  Encoders are not reset as the move is based on the current position.
     *  Move will stop if any of three conditions occur:
     *  1) Move gets to the desired position
     *  2) Move runs out of time
     *  3) Driver stops the opmode running.
     */
    private void encoderDrive(double speed,
                             double leftInches, double rightInches,
                             double timeoutS) {
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newLeftBackTarget;
        int newRightBackTarget;

        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newLeftFrontTarget = leftFrontDrive.getCurrentPosition() + (int)(leftInches * COUNTS_PER_INCH_HD_MECANUM);
            newRightFrontTarget = rightFrontDrive.getCurrentPosition() + (int)(rightInches * COUNTS_PER_INCH_HD_MECANUM);
            newLeftBackTarget = leftBackDrive.getCurrentPosition() + (int)(leftInches * COUNTS_PER_INCH_HD_MECANUM);
            newRightBackTarget = rightBackDrive.getCurrentPosition() + (int)(rightInches * COUNTS_PER_INCH_HD_MECANUM);
            leftFrontDrive.setTargetPosition(newLeftFrontTarget);
            rightFrontDrive.setTargetPosition(newRightFrontTarget);
            leftBackDrive.setTargetPosition(newLeftBackTarget);
            rightBackDrive.setTargetPosition(newRightBackTarget);

            // Turn On RUN_TO_POSITION
            leftFrontDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rightFrontDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            leftBackDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            rightBackDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();
            leftFrontDrive.setPower(Math.abs(speed));
            rightFrontDrive.setPower(Math.abs(speed));
            leftBackDrive.setPower(Math.abs(speed));
            rightBackDrive.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (leftFrontDrive.isBusy() && rightFrontDrive.isBusy() && leftBackDrive.isBusy() && rightBackDrive.isBusy())) {

                /*// Display it for the driver.
                telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                telemetry.addData("Path2",  "Running at %7d :%7d",
                        robot.leftDrive.getCurrentPosition(),
                        robot.rightDrive.getCurrentPosition());
                telemetry.update();*/
            }

            // Stop all motion;
            leftFrontDrive.setPower(0);
            rightFrontDrive.setPower(0);
            leftBackDrive.setPower(0);
            rightBackDrive.setPower(0);

            // Turn off RUN_TO_POSITION
            leftFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rightFrontDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            leftBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            rightBackDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    private void encoderRotateMotor(double speed,
                             DcMotor motor, double inches,
                             double timeoutS, double countsPerInch) {
        int newTarget;

        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newTarget = motor.getCurrentPosition() + (int)(inches * countsPerInch);
            motor.setTargetPosition(newTarget);

            // Turn On RUN_TO_POSITION
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();
            motor.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (motor.isBusy())) {

                /*// Display it for the driver.
                telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                telemetry.addData("Path2",  "Running at %7d :%7d",
                        robot.leftDrive.getCurrentPosition(),
                        robot.rightDrive.getCurrentPosition());
                telemetry.update();*/
            }

            // Stop all motion;
            motor.setPower(0);

            // Turn off RUN_TO_POSITION
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    protected void realEncoderRotateMotorToPosition(double speed,
                                                 DcMotor motor, int position,
                                                 double timeoutS) {
        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            motor.setTargetPosition(position);

            // Turn On RUN_TO_POSITION
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();
            motor.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (motor.isBusy())) {

                /*// Display it for the driver.
                telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                telemetry.addData("Path2",  "Running at %7d :%7d",
                        robot.leftDrive.getCurrentPosition(),
                        robot.rightDrive.getCurrentPosition());
                telemetry.update();*/

                telemetry.addData("rermtp", motor.getCurrentPosition() + " / " + position);
                telemetry.update();
            }

            // Stop all motion;
            motor.setPower(0);

            // Turn off RUN_TO_POSITION
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    private void realEncoderRotateMotor(double speed,
                                   DcMotor motor, int counts,
                                   double timeoutS) {
        int newTarget;

        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newTarget = motor.getCurrentPosition() + counts;
            motor.setTargetPosition(newTarget);

            // Turn On RUN_TO_POSITION
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();
            motor.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (motor.isBusy())) {

                /*// Display it for the driver.
                telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                telemetry.addData("Path2",  "Running at %7d :%7d",
                        robot.leftDrive.getCurrentPosition(),
                        robot.rightDrive.getCurrentPosition());
                telemetry.update();*/

            }

            // Stop all motion;
            motor.setPower(0);

            // Turn off RUN_TO_POSITION
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }


    private void encoderTurn(double speed, double degrees, double timeOutS) {
        double amountToTurn = degrees * TURN_DISTANCE_PER_DEGREE;
        if (amountToTurn == 0) {return;}
        encoderDrive(speed, amountToTurn, -amountToTurn, timeOutS);
    }

    protected void imuTurn(double speed, double degrees) {
        leftBackDrive.setPower(-speed);
        leftFrontDrive.setPower(-speed);
        rightFrontDrive.setPower(speed);
        rightBackDrive.setPower(speed);
        if (getHeading() < degrees) {
            while (getHeading() < degrees && opModeIsActive()) {
                telemetry.addData("imut", getHeading() + " / " + degrees);
                telemetry.update();
                idle();
            }
        }
        else {
            while (getHeading() > degrees && opModeIsActive()) {
                telemetry.addData("imut", getHeading() + " / " + degrees);
                telemetry.update();
                idle();
            }
        }
    }

    private void imuCorrectHeading(double speed, double degrees) {
        if (getHeading() < degrees) {
            leftBackDrive.setPower(-speed);
            leftFrontDrive.setPower(-speed);
            rightFrontDrive.setPower(speed);
            rightBackDrive.setPower(speed);
            while (getHeading() < degrees && opModeIsActive()) idle();
        }
        else {
            leftBackDrive.setPower(speed);
            leftFrontDrive.setPower(speed);
            rightFrontDrive.setPower(-speed);
            rightBackDrive.setPower(-speed);
            while (getHeading() > degrees && opModeIsActive()) idle();
        }
        leftBackDrive.setPower(0);
        leftFrontDrive.setPower(0);
        rightFrontDrive.setPower(0);
        rightBackDrive.setPower(0);
    }


    private void waitMs(double ms) {
        double targetMs = runtime.milliseconds() + ms;
        while (opModeIsActive() && runtime.milliseconds() < targetMs) {
            // do nothing
        }
    }

    /**
     * Initialize the Vuforia localization engine.
     */
    protected void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
//        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        parameters.cameraName = cameraName;

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }

    /**
     * Initialize the Tensor Flow Object Detection engine.
     */
    protected void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }

    protected int getGoldMineralPosAndWaitForStart() {
        int goldMineralPos = 2;
        while (/*opModeIsActive() && */! isStarted() && ! isStopRequested()) {
            if (tfod != null) {
                // getUpdatedRecognitions() will return null if no new information is available since
                // the last time that call was made.
                List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    telemetry.addData("# Object Detected", updatedRecognitions.size());
                    int goldMineralCount = 0;
                    for (Recognition recognition : updatedRecognitions) {
                        /* note: the following conditions mean:
                            recognition.getWidth() < recognition.getImageWidth() / 3
                                avoids a very wide false positive that can be caused by the background
                            recognition.getBottom() > recognition.getImageHeight() * 2 / 3
                                ignores any minerals in the crater
                            recognition.getWidth() < 1.5 * recognition.getHeight()
                                avoids a rectangular false positive generated by the red x
                        */
                        if (recognition.getWidth() < recognition.getImageWidth() / 3 &&
                                recognition.getBottom() > recognition.getImageHeight() * 2 / 3 &&
                                recognition.getWidth() < 1.5 * recognition.getHeight()) {
                            if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                goldMineralCount++;
                                if (recognition.getLeft() < recognition.getImageWidth() / 3) {
                                    goldMineralPos = 0;
                                }
                                else if (recognition.getLeft() < recognition.getImageWidth() / 3 * 2) {
                                    goldMineralPos = 1;
                                }
                                else {
                                    goldMineralPos = 2;
                                }
                            }
                        }
                    }
                    if (goldMineralCount <= 1) {
                        if (goldMineralPos == 0) {
                            telemetry.addData("Gold Mineral Position", "Left");
                        } else if (goldMineralPos == 1) {
                            telemetry.addData("Gold Mineral Position", "Center");
                        } else if (goldMineralPos == 2) {
                            telemetry.addData("Gold Mineral Position", "Right");
                        }
                    } else {
                        goldMineralPos = 2;
                    }
                    telemetry.update();
                }
            }
        }
        return goldMineralPos;
    }

    private float getHeading() {
        Orientation oreo = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        return oreo.firstAngle - imuInitialHeading;
    }
}
