package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
//import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
//import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

@Autonomous(name = "TensorFlowExperimentTwo", group = "Linear OpMode")
public class TensorFlowExperimentTwo extends ParameterizedAutoOp {
    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";
    private static final String VUFORIA_KEY = "ATpx/+X/////AAABmfb8JPlVBE36u1aOuMQwZNonagKlWLZlN8El54CvVoSwP8/jRizuHUxhU3ZqEEVXaImHF3XrM9cVIAEEj240Wr1FpNwfVcyBrYnDk5O8f5qPSVBLuURXxOvdAB8lP/yC3qPVFN5LKM4XkFBF1JPUHYpOR/O7Vb9JCcubzP8Xi4+qV5jdT9cb5qaUQRzDnnCjkRlZRc10ZpIVIeyerC5W0S+rSAAo9d1StcRyyAaebnpYRCqhZ5C+PCwGARuTSwCRdz424bfPK8G5XZ82Mtklufuf7w7uDDxBg1yoxYhtJNa8TD5bEK7VTPsNYQ0bUGZoD90Ga6HhGwyv/ofKH/3afMJouu0FnzAt1QDhGR4nPU1H";

//    private VuforiaLocalizer vuforia;
//
//    private TFObjectDetector tfod;

    @Override
    public void runOpMode() {
        cameraName = hardwareMap.get(CameraName.class, "Webcam");

        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }
        tfod.activate();
        getGoldMineralPosAndWaitForStart();
        if (! opModeIsActive()) return;
        tfod.deactivate();

    }
}
